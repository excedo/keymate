<nav class="navbar navbar-inverse nav-font bg-comp-0 clearfix">
    <div class "align-left navbar-brand"><img src="./img/VantaanAutomyynti-white.png" class="img-rounded img-responsive" width="300px" alt="" class="align-top"></div>
    <div class="align-right navbar-brand"><h3 class="h3 d-inline-block align-right"><?php echo $pritek?></h3></div>
      <!-- <img src="./img/VantaanAutomyynti-white.png" class="img-rounded img-responsive" width="450" alt="Vantaan Automyynti logo">
      <h2 class="navbar-brand h2"><?php echo $pritek?></h2> -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li id="nav_li_home" class=""><a href="?page=index"><h3>Pihavarasto</h3></a></li>
        <li id="nav_li_hall" class=""><a href="?page=hallinnoi"><h3>Autojen hallinta</h3></a></li>
      </ul>
    </div>

</nav>
