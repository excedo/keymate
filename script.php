<script>
function delRow(id) {
  //alert(id);
  $("#del_button" + id).button('loading');
  if ('undefined' != typeof id) {
    $.get('getdeleterow.php?remove=' + id, function() {}).fail(function() {
      //alert('VIRHE: Yhteysvirhe, yritä uudelleen.')
    });
  } else; //alert('VIRHE: Yhteysvirhe, yritä uudelleen.');
  //alert('Auto poistettu varastosta.')
  window.location.replace("index.php?page=hallinnoi");
  event.stopPropagation();
}

function myyAuto(id) {
  //  alert(id);
  if ('undefined' != typeof id) {
    $.get('getdeleterow.php?remove=' + id, function() {}).fail(function() {
      alert('VIRHE: Yhteysvirhe, yritä uudelleen.')
    });
  } else alert('VIRHE: Yhteysvirhe, yritä uudelleen.');
  alert('Auto myyty ja poistettu varastosta.')
  window.location.replace("index.php");
  event.stopPropagation();
}

$(document).ready(function() {
  $("#lisaa_auto_form").submit(function(event) {
    event.preventDefault();
    var rekkari = $("#rekkari").val();
    var autopaikka = $("#autopaikka").val();
    var avaimia = $("#lisaa_auto_avaimia").val();
    var status = $("#status").val();
    var dataString = 'rekkari1=' + rekkari + '&autopaikka1=' + autopaikka + '&status1=' + status + '&avaimia1=' + avaimia;
    //alert (autopaikka);
    if ( autopaikka ) {
      if (rekkari == '' || autopaikka == '99') {
        alert("Rekisterinumero ei voi olla tyhjä");
        $("#lisaa_auto_btn").button('reset');
      } else if (autopaikka == '99') {
        alert("Autorivi ei voi olla tyhjä");
        $("#lisaa_auto_btn").button('reset');
      } else {
        // AJAX Code To Submit Form.
        $.ajax({
          type: "POST",
          url: "lisaa_auto.php",
          data: dataString,
          cache: false,
          success: function(result) {
            $("#lisaa_auto").modal("hide");
            $("#nayta_apaikka").modal("show");
            add_apaikka();
          }
        });
      }
      return false;
    } else {
      alert("Autorivi ei voi tyhjä!");
      $("#lisaa_auto_btn").button('reset');
      return false;
  }
  });
});


$(document).ready(function() {
  $("#paluu_form").submit(function(event) {
    event.preventDefault();
    var autopaikka = $("#paluu_autopaikka").val();
    var status = $("#paluu_status").val();
    var paluu_id = $("#paluu_id").val();
    // Returns successful data submission message when the entered information is stored in database.
    var dataString = 'paluu_id=' + paluu_id + '&paluu_autopaikka=' + autopaikka + '&paluu_status=' + status;
    if (status == '' || autopaikka == '') {
      alert("Ei voi olla tyhjä");
      $("#paluu_save_btn").button('reset');
    } else if (autopaikka == '') {
      alert("Autorivi ei voi olla tyhjä");
      $("#paluu_save_btn").button('reset');
    } else {
      // AJAX Code To Submit Form.
      $.ajax({
        type: "POST",
        url: "getpaluumodal.php",
        data: dataString,
        cache: false,
        success: function(result) {
          $("#paluu_modal").modal("hide");
          $("#nayta_apaikka").modal("show");
          //alert (paluu_id);
          paluumodal(paluu_id);
        }
      });
    }
    return false;
  });
});

function add_apaikka() {
  //console.log("add_apaikka");
  $.getJSON('addmodal.php?add=1', function(dab) {
    dab = dab[0];
    //console.log(dab);
    $("#nayta_apaikka_spinner").remove();
    $("#nayta_apaikka_loading").html("");
    $("#add_rek").text(dab.reknr);
    $("#add_paikka").text(dab.avainpaikka);
    $("#add_kaappi").text(dab.autopaikka);
  });
};

function koeajoon(id) {
  //console.log("koeajoon " + id);
  //alert("koeajoon " + id);
    // Returns successful data submission message when the entered information is stored in database.
    $("#koeajo_btn" + id).button('loading');
    var dataString = 'edit-id=' + id + '&status=Koeajossa';
      // AJAX Code To Submit Form.
      $.ajax({
        type: "POST",
        url: "geteditmodal.php",
        data: dataString,
        cache: false,
        success: function(result) {
          window.location.replace("index.php");
                event.stopPropagation();
        }
      });
    return false;
    event.stopPropagation();
  };


function korjaamolle(id) {
  //console.log("korjaamolle " + id);
  // Returns successful data submission message when the entered information is stored in database.
  $("#korjaamo_btn" + id).button('loading');
  var dataString = 'edit-id=' + id + '&status=Korjaamolla';
    // AJAX Code To Submit Form.
    $.ajax({
      type: "POST",
      url: "geteditmodal.php",
      data: dataString,
      cache: false,
      success: function(result) {
        window.location.replace("index.php");
              event.stopPropagation();
      }
    });
  return false;
  event.stopPropagation();
};

function paluu(id) {
  //console.log("paluu " + id);
    $("#paluu_btn" + id).button('loading');
    if ('undefined' != typeof id) {
      $.getJSON('geteditmodal.php?edit=' + id, function(obj) {
        obj = obj[0]
        //console.log(obj)
        $("#paluu_print_rek").text(obj.reknr);
        $("#paluu_print_kja").text(obj.avainpaikka);
        $("#paluu_print_kap").text(obj.autopaikka);
        $("input[id=paluu_id]").val(obj.id);
        $("input[id=autopaikka]").val(obj.autopaikka);
        $("select[id=status]").val(obj.status);
        $('#paluu_modal').modal('show')
      }).fail(function() {
        //console.log('Unable to fetch data, please try again later.')
      });
    } else; //console.log('Unknown row id.');
};

function add_apaikka2() {
  //console.log("add_apaikka");
  $.getJSON('addmodal.php?add=1', function(dab) {
    dab = dab[0];
    //console.log(dab);
    $("#add_rek").text(dab.reknr);
    $("#add_paikka").text(dab.avainpaikka);
    $("#add_kaappi").text(dab.autopaikka);
    $("#nayta_apaikka").modal("show");
  });
};

function paluumodal(id) {
  console.log("paluu " + id);
  $.getJSON('addmodal.php?paluu=1&paluu_id=' + id, function(dab) {
    dab = dab[0];
    //console.log(dab);
    $("#nayta_apaikka_spinner").remove();
    $("#nayta_apaikka_loading").html("");
    $("#add_rek").text(dab.reknr);
    $("#add_paikka").text(dab.avainpaikka);
    $("#add_kaappi").text(dab.autopaikka);
    //alert (id);
  })
};

function editRow(id) {
  console.log(id);
  if ('undefined' != typeof id) {
    $.getJSON('geteditmodal.php?edit=' + id, function(obj) {
      obj = obj[0]
      console.log(obj)
      $("#edit_print_rek").text(obj.reknr);
      $("#edit_print_kja").text(obj.avainpaikka);
      $("#edit_print_kap").text(obj.autopaikka);
      $("input[id=edit-id]").val(obj.id);
      $("input[id=autopaikka]").val(obj.autopaikka);
      $("select[id=status]").val(obj.status);
      $('#edit').modal('show')
    }).fail(function() {
      //console.log('Unable to fetch data, please try again later.')
    });
  } //else console.log('Unknown row id.');
}

$("#iso_lisaa_auto_btn").click(function ()
{
     $(this).button('loading');
}
);

$("#lisaa_auto_btn").click(function ()
{
     $(this).button('loading');
}
);

$("#paluu_save_btn").click(function ()
{
     $(this).button('loading');
}
);
// Every time a modal is shown, if it has an autofocus element, focus on it.
$('.modal').on('shown.bs.modal', function() {
  $(this).find('[autofocus]').focus();
});
</script>
