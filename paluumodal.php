<?php //paluu modal ?>
<div id="paluu_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h4 class="modal-title">Palauta auto varastoon</h4></div>
            <div class="col-sm-8">
              <h2><span id="paluu_print_rek"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <form action="./getpaluumodal.php" method="post" class=".form-horizontal">
          <div class="form-group hidden">
            <input type="text" class="form-control hidden" name="paluu_id" id="paluu_id" value="">
          </div>
          <div class="form-group">
            <label for="autopaikka">Autorivi</label>
            <select class="form-control" name="paluu_autopaikka" id="paluu_autopaikka">
                      <option value="" selected disabled hidden>Valitse autorivi</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
          </div>
          <div class="form-group hidden">
                    <input class="form-control hidden" name="paluu_status" id="paluu_status" value="Varastossa">

          </div>
          <div align="center">
            <button type="submit" name="submit" class="btn btn-success btn-block" id="paluu_save_btn"><span class="glyphicon glyphicon-ok"></span> Auto varastoon</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
</div>
