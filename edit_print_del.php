<?php //edit modal ?>
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h4 class="modal-title">Vaihda tilaa / Siirrä</h4></div>
            <div class="col-sm-8">
              <h2><span id="edit_print_rek"></h2>
              <h2>Kaappi <span id="edit_print_kap"></span> Avainpaikka <span id="edit_print_kja"></span></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <form action="./geteditmodal.php" method="post" class=".form-horizontal">
          <div class="form-group hidden">
            <input type="text" class="form-control hidden" name="edit-id" id="edit-id" value="">
          </div>
          <div class="form-group">
            <label for="autopaikka">Autorivi</label>
            <select class="form-control" name="autopaikka" id="autopaikka">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
          </div>
          <div class="form-group">
            <label for="status">Tila</label>
            <select class="form-control" name="status" id="status">
                    <option value="Varastossa">Varastossa</option>
                    <option value="Korjaamolla">Korjaamolla</option>
                    <option value="Koeajossa">Koeajossa</option>
                  </select>
          </div>
          <div align="center">
            <button type="submit" name="submit" class="btn btn-info btn-block" id="edit_save_btn"><span class="glyphicon glyphicon-ok"></span> Tallenna</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
</div>
