<?php
	// set root path based on where index.php is
	// this could be used to set a dev path too
	define("ROOT_PATH", dirname(__DIR__) . '/');
	// // load up your config file
  	 //   require_once(__DIR__ . '/config.php');
		 // load up medoo
		 require_once(__DIR__ . "/Medoo.php");
     // load up your db connection module
     require_once(__DIR__ . '/db.php');
     include('login.php'); // Includes Login Script

if(isset($_SESSION['login_user'])){
header("location: profile.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Login Form in PHP with Session</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="main">
<h1>PHP Login Session Example</h1>
<div id="login">
<h2>Login Form</h2>
<form action="" method="post">
<label>UserName :</label>
<input id="name" name="username" placeholder="username" type="text">
<label>Password :</label>
<input id="password" name="password" placeholder="**********" type="password">
<input name="submit" type="submit" value=" Login ">
<span><?php echo $error; ?></span>
</form>
</div>
</div>
</body>
</html>
