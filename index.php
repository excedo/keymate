<?php
include "./config.php";
if ($error == "1") {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
	// set root path based on where index.php is
	// this could be used to set a dev path too
	define("ROOT_PATH", dirname(__DIR__) . '/');
	// // load up your config file
  	 //   require_once(__DIR__ . '/config.php');
		 // load up medoo
		 require_once(__DIR__ . "/Medoo.php");
     // load up your db connection module
     require_once(__DIR__ . '/db.php');



?>


<!DOCTYPE html>
<html lang="fi">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Delius|Josefin+Sans|Josefin+Slab|Tangerine" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="./css/colors.css">
		<link rel="stylesheet" href="./css/override.css">
	  <script src="./js/search-table.js"></script>
	  <script src="./js/send.js"></script>
		<!-- favicon -->
					<link rel="apple-touch-icon" sizes="57x57" href="./fav/apple-icon-57x57.png">
					<link rel="apple-touch-icon" sizes="60x60" href="./fav/apple-icon-60x60.png">
					<link rel="apple-touch-icon" sizes="72x72" href="./fav/apple-icon-72x72.png">
					<link rel="apple-touch-icon" sizes="76x76" href="./fav/apple-icon-76x76.png">
					<link rel="apple-touch-icon" sizes="114x114" href="./fav/apple-icon-114x114.png">
					<link rel="apple-touch-icon" sizes="120x120" href="./fav/apple-icon-120x120.png">
					<link rel="apple-touch-icon" sizes="144x144" href="./fav/apple-icon-144x144.png">
					<link rel="apple-touch-icon" sizes="152x152" href="./fav/apple-icon-152x152.png">
					<link rel="apple-touch-icon" sizes="180x180" href="./fav/apple-icon-180x180.png">
					<link rel="icon" type="image/png" sizes="192x192"  href="./fav/android-icon-192x192.png">
					<link rel="icon" type="image/png" sizes="32x32" href="./fav/favicon-32x32.png">
					<link rel="icon" type="image/png" sizes="96x96" href="./fav/favicon-96x96.png">
					<link rel="icon" type="image/png" sizes="16x16" href="./fav/favicon-16x16.png">
					<link rel="manifest" href="./fav/manifest.json">
					<meta name="msapplication-TileColor" content="#ffffff">
					<meta name="msapplication-TileImage" content="./fav/ms-icon-144x144.png">
					<meta name="theme-color" content="#ffffff">
		<title>Autojen Pihavarastohallinta</title>
  </head>
	<body class="bg-body">

  <?php
  $page = @$_GET['page'];
	$act = @$_GET['a'];

  if($page == "index"){
  include"home.php";
  }
  elseif($page == "hallinnoi"){
  include"autohallinta.php";
  }
  elseif($page == "siirra_auto"){
  include"siirra_auto.php";
  }
  else{ include"home.php";
  }
  ?>

</body>
<?php include "./script.php" ?>
	</html>
