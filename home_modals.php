  <?php // adding car modal
  //
  //
  //
  //
  //
  // ?>
  <div id="lisaa_auto" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Lisää Auto</h4>
        </div>
        <div class="modal-body">
          <form class=".form-horizontal" id="lisaa_auto_form" autocomplete="off">
              <div class="form-group">
              <label for="rekkari">Rekisterinumero</label>
              <input type="text" class="form-control" name="rekkari" id="rekkari" autofocus placeholder="Syötä rekisterinumero abc123 / abc-123 / ABC-123 / ABC123">
            </div>
            <div class="form-group">
              <label for="autopaikka">Autorivi</label>
              <select class="form-control" name="autopaikka" id="autopaikka">
                <option value="" selected disabled hidden>Valitse autorivi</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
            <div>
              <label for="Avaimia">Avaimia</label>
              <select class="form-control" name="lisaa_auto_avaimia" id="lisaa_auto_avaimia">
                <option selected value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5+</option>
              </select>
            </div>
            <div class="form-group">
              <input type="hidden" class="form-control" name="status" id="status" value="Varastossa">
            </div>
            <button type="submit" value="submit" class="btn btn-success btn-block" data-loading-text="<span class='glyphicon-left glyphicon glyphicon-refresh spinning'</span>" id="lisaa_auto_btn"><span class="glyphicon glyphicon-plus"></span> Lisää Auto</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" name="close" class="btn btn-default" data-dismiss="modal">Sulje</button>
        </div>
      </div>
    </div>
    <script>
    $('#lisaa_auto').on('hidden.bs.modal', function (e) {
     $("#iso_lisaa_auto_btn").button('reset');
    })
    </script>
  </div>
<!-- </div> -->
<?php // show avainpaikka after adding car
//
//
//
//
//
// ?>
<div id="nayta_apaikka" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title"><span id="nayta_apaikka_spinner" class='glyphicon-left glyphicon glyphicon-refresh spinning'></span><span id="nayta_apaikka_loading"> Ladataan tietoja...</span><span id="add_rek"> avainpaikka on</span></h2>
      </div>
      <div class="modal-body">
        <h4>Kaappi <span id="add_kaappi"></span>  Paikka <span id="add_paikka"></span></h4>
        </div>
      <div class="modal-footer">
        <button type="button" name="submit" class="btn btn-default" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
  <script>
  $('#nayta_apaikka').on('hidden.bs.modal', function (e) {
  window.location.replace("index.php");
  })
  </script>
</div>

<?php //edit modal
//
//
//
//
//?>
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h4 class="modal-title">Vaihda tilaa / Siirrä</h4></div>
            <div class="col-sm-8">
              <h2><span id="edit_print_rek"></h2>
              <h2>Kaappi <span id="edit_print_kap"></span> Avainpaikka <span id="edit_print_kja"></span></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <form action="./geteditmodal.php" method="post" class=".form-horizontal">
          <div class="form-group hidden">
            <input type="text" class="form-control hidden" name="edit-id" id="edit-id" value="">
          </div>
          <div class="form-group">
            <label for="autopaikka">Autorivi</label>
            <select class="form-control" name="autopaikka" id="autopaikka">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
          </div>
          <div class="form-group">
            <label for="status">Tila</label>
            <select class="form-control" name="status" id="status">
                    <option value="Varastossa">Varastossa</option>
                    <option value="Korjaamolla">Korjaamolla</option>
                    <option value="Koeajossa">Koeajossa</option>
                  </select>
          </div>
          <div align="center">
            <button type="submit" name="submit" class="btn btn-info btn-block" id="edit_save_btn"><span class="glyphicon glyphicon-ok"></span> Tallenna</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
</div>

<?php //paluu modal
//
//
//
//
//
//?>
<div id="paluu_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h4 class="modal-title">Palauta auto varastoon</h4></div>
            <div class="col-sm-8">
              <h2><span id="paluu_print_rek"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <form id="paluu_form" class=".form-horizontal" >
        <!-- <form action="./getpaluumodal.php" method="post" class=".form-horizontal"> -->
          <div class="form-group hidden">
            <input type="text" class="form-control hidden" name="paluu_id" id="paluu_id" value="">
          </div>
          <div class="form-group">
            <label for="autopaikka">Autorivi</label>
            <select class="form-control" name="paluu_autopaikka" id="paluu_autopaikka" autofocus>
                      <option value="non_selected" selected disabled hidden>Valitse autorivi</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
          </div>
          <div class="form-group hidden">
                    <input class="form-control hidden" name="paluu_status" id="paluu_status" value="Varastossa">

          </div>
          <div align="center">
            <button type="submit" name="submit" class="btn btn-success btn-block" data-loading-text="<span class='glyphicon-left glyphicon glyphicon-refresh spinning'</span>" id="paluu_save_btn"><span class="glyphicon glyphicon-ok"></span> Auto varastoon</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
  <script>
  $('#paluu_modal').on('hidden.bs.modal', function (e) {
  $(".btn-success").button('reset');
  })
  </script>
</div>
