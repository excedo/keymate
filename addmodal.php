<?php
require_once(__DIR__ . "/Medoo.php");
require_once(__DIR__ . '/db.php');
require_once(__DIR__ . '/config.php');
if ( isset($_GET['add']) && 0 < intval($_GET['add']) ) {
  $nayta_last_id = $db->max($db_auto, "id");
  //echo $nayta_last_id;
  $modaladd_data = $db->select($db_auto, [
        'reknr',
        'autopaikka',
        'avainpaikka'
        ],[
        'id' => $nayta_last_id
        ]);
$myJSON = json_encode($modaladd_data);
die($myJSON);
}
if ( isset($_GET['paluu']) && 0 < intval($_GET['paluu']) ) {
  $paluu_id = $_GET['paluu_id'];
  //echo $paluu_id;
  $modalpaluu_data = $db->select($db_auto, [
        'reknr',
        'autopaikka',
        'avainpaikka'
        ],[
        'id' => $paluu_id
        ]);
        //var_dump( $db->error() );
$myJSON = json_encode($modalpaluu_data);
die($myJSON);
}
?>
