-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2017 at 05:12 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autovar`
--

-- --------------------------------------------------------

--
-- Table structure for table `auto`
--

CREATE TABLE `auto` (
  `id` int(11) NOT NULL,
  `reknr` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `merkki` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `malli` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `autopaikka` int(5) DEFAULT NULL,
  `avainpaikka` varchar(7) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `auto`
--

INSERT INTO `auto` (`id`, `reknr`, `merkki`, `malli`, `autopaikka`, `avainpaikka`, `status`, `deleted`) VALUES
(1, 'ABC-001', NULL, NULL, 1, '1', 'Varastossa', 0),
(2, 'ABC-002', NULL, NULL, 1, '2', 'Varastossa', 0),
(3, 'AB', NULL, NULL, 1, '3', 'Varastossa', 0),
(4, 'ABC-003', NULL, NULL, 1, '4', 'Varastossa', 0),
(5, 'ABC-004', NULL, NULL, 1, '5', 'Varastossa', 0),
(6, 'ABC-005', NULL, NULL, 1, '6', 'Varastossa', 0),
(7, 'ABC-006', NULL, NULL, 1, '7', 'Varastossa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `automerkki`
--

CREATE TABLE `automerkki` (
  `id` int(11) NOT NULL,
  `automerkki` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `del` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `automerkki`
--

INSERT INTO `automerkki` (`id`, `automerkki`, `del`) VALUES
(1, 'Alfa Romeo', NULL),
(2, 'Aston Martin ', NULL),
(3, 'Audi', NULL),
(4, 'Bentley', NULL),
(5, 'BMW', NULL),
(6, 'Chevrolet', NULL),
(7, 'Chrysler', NULL),
(8, 'Citroën', NULL),
(9, 'Dacia', NULL),
(10, 'Daewoo', NULL),
(11, 'Daihatsu', NULL),
(12, 'Dodge', NULL),
(13, 'Ferrari', NULL),
(14, 'Fiat', NULL),
(15, 'Ford', NULL),
(16, 'GMC', NULL),
(17, 'Honda', NULL),
(18, 'Hyundai', NULL),
(19, 'Isuzu', NULL),
(20, 'Jaguar', NULL),
(21, 'Jeep', NULL),
(22, 'Kia', NULL),
(23, 'Lada', NULL),
(24, 'Lamborghini', NULL),
(25, 'Lancia', NULL),
(26, 'Land Rover', NULL),
(27, 'Lexus', NULL),
(28, 'Lincoln', NULL),
(29, 'Lotus', NULL),
(30, 'Maserati', NULL),
(31, 'Maybach', NULL),
(32, 'Mazda', NULL),
(33, 'Mercedes-Benz', NULL),
(34, 'Mini', NULL),
(35, 'Mitsubishi', NULL),
(36, 'Nissan', NULL),
(37, 'Opel', NULL),
(38, 'Peugeot', NULL),
(39, 'Porsche', NULL),
(40, 'Renault', NULL),
(41, 'Rolls-Royce', NULL),
(42, 'Rover', NULL),
(43, 'Saab', NULL),
(44, 'Seat', NULL),
(45, 'Skoda', NULL),
(46, 'Smart', NULL),
(47, 'Subaru', NULL),
(48, 'Suzuki', NULL),
(49, 'Toyota', NULL),
(50, 'Volkswagen', NULL),
(51, 'Volvo', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `avainpaikat`
--

CREATE TABLE `avainpaikat` (
  `id` int(11) NOT NULL,
  `kaappi` int(3) DEFAULT NULL,
  `avainpaikka` int(3) DEFAULT NULL,
  `avaintila` tinyint(1) DEFAULT NULL,
  `auto_id` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `avainpaikat`
--

INSERT INTO `avainpaikat` (`id`, `kaappi`, `avainpaikka`, `avaintila`, `auto_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 2, 1, 2),
(3, 1, 3, 1, 3),
(4, 1, 4, 1, 4),
(5, 1, 5, 1, 5),
(6, 1, 6, 1, 6),
(7, 1, 7, 1, 7),
(8, 1, 8, 0, 0),
(9, 1, 9, 0, 0),
(10, 1, 10, 0, 0),
(11, 1, 11, 0, 0),
(12, 1, 12, 0, 0),
(13, 1, 13, 0, 0),
(14, 1, 14, 0, 0),
(15, 1, 15, 0, 0),
(16, 1, 16, 0, 0),
(17, 1, 17, 0, 0),
(18, 1, 18, 0, 0),
(19, 1, 19, 0, 0),
(20, 1, 20, 0, 0),
(21, 1, 21, 0, 0),
(22, 1, 22, 0, 0),
(23, 1, 23, 0, 0),
(24, 1, 24, 0, 0),
(25, 1, 25, 0, 0),
(26, 1, 26, 0, 0),
(27, 1, 27, 0, 0),
(28, 1, 28, 0, 0),
(29, 1, 29, 0, 0),
(30, 1, 30, 0, 0),
(31, 2, 1, 0, 0),
(32, 2, 2, 0, 0),
(33, 2, 3, 0, 0),
(34, 2, 4, 0, 0),
(35, 2, 5, 0, 0),
(36, 2, 6, 0, 0),
(37, 2, 7, 0, 0),
(38, 2, 8, 0, 0),
(39, 2, 9, 0, 0),
(40, 2, 10, 0, 0),
(41, 2, 11, 0, 0),
(42, 2, 12, 0, 0),
(43, 2, 13, 0, 0),
(44, 2, 14, 0, 0),
(45, 2, 15, 0, 0),
(46, 2, 16, 0, 0),
(47, 2, 17, 0, 0),
(48, 2, 18, 0, 0),
(49, 2, 19, 0, 0),
(50, 2, 20, 0, 0),
(51, 2, 21, 0, 0),
(52, 2, 22, 0, 0),
(53, 2, 23, 0, 0),
(54, 2, 24, 0, 0),
(55, 2, 25, 0, 0),
(56, 2, 26, 0, 0),
(57, 2, 27, 0, 0),
(58, 2, 28, 0, 0),
(59, 2, 29, 0, 0),
(60, 2, 30, 0, 0),
(61, 3, 1, 0, 0),
(62, 3, 2, 0, 0),
(63, 3, 3, 0, 0),
(64, 3, 4, 0, 0),
(65, 3, 5, 0, 0),
(66, 3, 6, 0, 0),
(67, 3, 7, 0, 0),
(68, 3, 8, 0, 0),
(69, 3, 9, 0, 0),
(70, 3, 10, 0, 0),
(71, 3, 11, 0, 0),
(72, 3, 12, 0, 0),
(73, 3, 13, 0, 0),
(74, 3, 14, 0, 0),
(75, 3, 15, 0, 0),
(76, 3, 16, 0, 0),
(77, 3, 17, 0, 0),
(78, 3, 18, 0, 0),
(79, 3, 19, 0, 0),
(80, 3, 20, 0, 0),
(81, 3, 21, 0, 0),
(82, 3, 22, 0, 0),
(83, 3, 23, 0, 0),
(84, 3, 24, 0, 0),
(85, 3, 25, 0, 0),
(86, 3, 26, 0, 0),
(87, 3, 27, 0, 0),
(88, 3, 28, 0, 0),
(89, 3, 29, 0, 0),
(90, 3, 30, 0, 0),
(91, 4, 1, 0, 0),
(92, 4, 2, 0, 0),
(93, 4, 3, 0, 0),
(94, 4, 4, 0, 0),
(95, 4, 5, 0, 0),
(96, 4, 6, 0, 0),
(97, 4, 7, 0, 0),
(98, 4, 8, 0, 0),
(99, 4, 9, 0, 0),
(100, 4, 10, 0, 0),
(101, 4, 11, 0, 0),
(102, 4, 12, 0, 0),
(103, 4, 13, 0, 0),
(104, 4, 14, 0, 0),
(105, 4, 15, 0, 0),
(106, 4, 16, 0, 0),
(107, 4, 17, 0, 0),
(108, 4, 18, 0, 0),
(109, 4, 19, 0, 0),
(110, 4, 20, 0, 0),
(111, 4, 21, 0, 0),
(112, 4, 22, 0, 0),
(113, 4, 23, 0, 0),
(114, 4, 24, 0, 0),
(115, 4, 25, 0, 0),
(116, 4, 26, 0, 0),
(117, 4, 27, 0, 0),
(118, 4, 28, 0, 0),
(119, 4, 29, 0, 0),
(120, 4, 30, 0, 0),
(121, 5, 1, 0, 0),
(122, 5, 2, 0, 0),
(123, 5, 3, 0, 0),
(124, 5, 4, 0, 0),
(125, 5, 5, 0, 0),
(126, 5, 6, 0, 0),
(127, 5, 7, 0, 0),
(128, 5, 8, 0, 0),
(129, 5, 9, 0, 0),
(130, 5, 10, 0, 0),
(131, 5, 11, 0, 0),
(132, 5, 12, 0, 0),
(133, 5, 13, 0, 0),
(134, 5, 14, 0, 0),
(135, 5, 15, 0, 0),
(136, 5, 16, 0, 0),
(137, 5, 17, 0, 0),
(138, 5, 18, 0, 0),
(139, 5, 19, 0, 0),
(140, 5, 20, 0, 0),
(141, 5, 21, 0, 0),
(142, 5, 22, 0, 0),
(143, 5, 23, 0, 0),
(144, 5, 24, 0, 0),
(145, 5, 25, 0, 0),
(146, 5, 26, 0, 0),
(147, 5, 27, 0, 0),
(148, 5, 28, 0, 0),
(149, 5, 29, 0, 0),
(150, 5, 30, 0, 0),
(151, 6, 1, 0, 0),
(152, 6, 2, 0, 0),
(153, 6, 3, 0, 0),
(154, 6, 4, 0, 0),
(155, 6, 5, 0, 0),
(156, 6, 6, 0, 0),
(157, 6, 7, 0, 0),
(158, 6, 8, 0, 0),
(159, 6, 9, 0, 0),
(160, 6, 10, 0, 0),
(161, 6, 11, 0, 0),
(162, 6, 12, 0, 0),
(163, 6, 13, 0, 0),
(164, 6, 14, 0, 0),
(165, 6, 15, 0, 0),
(166, 6, 16, 0, 0),
(167, 6, 17, 0, 0),
(168, 6, 18, 0, 0),
(169, 6, 19, 0, 0),
(170, 6, 20, 0, 0),
(171, 6, 21, 0, 0),
(172, 6, 22, 0, 0),
(173, 6, 23, 0, 0),
(174, 6, 24, 0, 0),
(175, 6, 25, 0, 0),
(176, 6, 26, 0, 0),
(177, 6, 27, 0, 0),
(178, 6, 28, 0, 0),
(179, 6, 29, 0, 0),
(180, 6, 30, 0, 0),
(181, 7, 1, 0, 0),
(182, 7, 2, 0, 0),
(183, 7, 3, 0, 0),
(184, 7, 4, 0, 0),
(185, 7, 5, 0, 0),
(186, 7, 6, 0, 0),
(187, 7, 7, 0, 0),
(188, 7, 8, 0, 0),
(189, 7, 9, 0, 0),
(190, 7, 10, 0, 0),
(191, 7, 11, 0, 0),
(192, 7, 12, 0, 0),
(193, 7, 13, 0, 0),
(194, 7, 14, 0, 0),
(195, 7, 15, 0, 0),
(196, 7, 16, 0, 0),
(197, 7, 17, 0, 0),
(198, 7, 18, 0, 0),
(199, 7, 19, 0, 0),
(200, 7, 20, 0, 0),
(201, 7, 21, 0, 0),
(202, 7, 22, 0, 0),
(203, 7, 23, 0, 0),
(204, 7, 24, 0, 0),
(205, 7, 25, 0, 0),
(206, 7, 26, 0, 0),
(207, 7, 27, 0, 0),
(208, 7, 28, 0, 0),
(209, 7, 29, 0, 0),
(210, 7, 30, 0, 0),
(211, 8, 1, 0, 0),
(212, 8, 2, 0, 0),
(213, 8, 3, 0, 0),
(214, 8, 4, 0, 0),
(215, 8, 5, 0, 0),
(216, 8, 6, 0, 0),
(217, 8, 7, 0, 0),
(218, 8, 8, 0, 0),
(219, 8, 9, 0, 0),
(220, 8, 10, 0, 0),
(221, 8, 11, 0, 0),
(222, 8, 12, 0, 0),
(223, 8, 13, 0, 0),
(224, 8, 14, 0, 0),
(225, 8, 15, 0, 0),
(226, 8, 16, 0, 0),
(227, 8, 17, 0, 0),
(228, 8, 18, 0, 0),
(229, 8, 19, 0, 0),
(230, 8, 20, 0, 0),
(231, 8, 21, 0, 0),
(232, 8, 22, 0, 0),
(233, 8, 23, 0, 0),
(234, 8, 24, 0, 0),
(235, 8, 25, 0, 0),
(236, 8, 26, 0, 0),
(237, 8, 27, 0, 0),
(238, 8, 28, 0, 0),
(239, 8, 29, 0, 0),
(240, 8, 30, 0, 0),
(241, 9, 1, 0, 0),
(242, 9, 2, 0, 0),
(243, 9, 3, 0, 0),
(244, 9, 4, 0, 0),
(245, 9, 5, 0, 0),
(246, 9, 6, 0, 0),
(247, 9, 7, 0, 0),
(248, 9, 8, 0, 0),
(249, 9, 9, 0, 0),
(250, 9, 10, 0, 0),
(251, 9, 11, 0, 0),
(252, 9, 12, 0, 0),
(253, 9, 13, 0, 0),
(254, 9, 14, 0, 0),
(255, 9, 15, 0, 0),
(256, 9, 16, 0, 0),
(257, 9, 17, 0, 0),
(258, 9, 18, 0, 0),
(259, 9, 19, 0, 0),
(260, 9, 20, 0, 0),
(261, 9, 21, 0, 0),
(262, 9, 22, 0, 0),
(263, 9, 23, 0, 0),
(264, 9, 24, 0, 0),
(265, 9, 25, 0, 0),
(266, 9, 26, 0, 0),
(267, 9, 27, 0, 0),
(268, 9, 28, 0, 0),
(269, 9, 29, 0, 0),
(270, 9, 30, 0, 0),
(271, 9, 1, 0, 0),
(272, 9, 2, 0, 0),
(273, 9, 3, 0, 0),
(274, 9, 4, 0, 0),
(275, 9, 5, 0, 0),
(276, 9, 6, 0, 0),
(277, 9, 7, 0, 0),
(278, 9, 8, 0, 0),
(279, 9, 9, 0, 0),
(280, 9, 10, 0, 0),
(281, 9, 11, 0, 0),
(282, 9, 12, 0, 0),
(283, 9, 13, 0, 0),
(284, 9, 14, 0, 0),
(285, 9, 15, 0, 0),
(286, 9, 16, 0, 0),
(287, 9, 17, 0, 0),
(288, 9, 18, 0, 0),
(289, 9, 19, 0, 0),
(290, 9, 20, 0, 0),
(291, 9, 21, 0, 0),
(292, 9, 22, 0, 0),
(293, 9, 23, 0, 0),
(294, 9, 24, 0, 0),
(295, 9, 25, 0, 0),
(296, 9, 26, 0, 0),
(297, 9, 27, 0, 0),
(298, 9, 28, 0, 0),
(299, 9, 29, 0, 0),
(300, 9, 30, 0, 0),
(301, 10, 1, 0, 0),
(302, 10, 2, 0, 0),
(303, 10, 3, 0, 0),
(304, 10, 4, 0, 0),
(305, 10, 5, 0, 0),
(306, 10, 6, 0, 0),
(307, 10, 7, 0, 0),
(308, 10, 8, 0, 0),
(309, 10, 9, 0, 0),
(310, 10, 10, 0, 0),
(311, 10, 11, 0, 0),
(312, 10, 12, 0, 0),
(313, 10, 13, 0, 0),
(314, 10, 14, 0, 0),
(315, 10, 15, 0, 0),
(316, 10, 16, 0, 0),
(317, 10, 17, 0, 0),
(318, 10, 18, 0, 0),
(319, 10, 19, 0, 0),
(320, 10, 20, 0, 0),
(321, 10, 21, 0, 0),
(322, 10, 22, 0, 0),
(323, 10, 23, 0, 0),
(324, 10, 24, 0, 0),
(325, 10, 25, 0, 0),
(326, 10, 26, 0, 0),
(327, 10, 27, 0, 0),
(328, 10, 28, 0, 0),
(329, 10, 29, 0, 0),
(330, 10, 30, 0, 0),
(331, 11, 1, 0, 0),
(332, 11, 2, 0, 0),
(333, 11, 3, 0, 0),
(334, 11, 4, 0, 0),
(335, 11, 5, 0, 0),
(336, 11, 6, 0, 0),
(337, 11, 7, 0, 0),
(338, 11, 8, 0, 0),
(339, 11, 9, 0, 0),
(340, 11, 10, 0, 0),
(341, 11, 11, 0, 0),
(342, 11, 12, 0, 0),
(343, 11, 13, 0, 0),
(344, 11, 14, 0, 0),
(345, 11, 15, 0, 0),
(346, 11, 16, 0, 0),
(347, 11, 17, 0, 0),
(348, 11, 18, 0, 0),
(349, 11, 19, 0, 0),
(350, 11, 20, 0, 0),
(351, 11, 21, 0, 0),
(352, 11, 22, 0, 0),
(353, 11, 23, 0, 0),
(354, 11, 24, 0, 0),
(355, 11, 25, 0, 0),
(356, 11, 26, 0, 0),
(357, 11, 27, 0, 0),
(358, 11, 28, 0, 0),
(359, 11, 29, 0, 0),
(360, 11, 30, 0, 0),
(361, 11, 1, 0, 0),
(362, 11, 2, 0, 0),
(363, 11, 3, 0, 0),
(364, 11, 4, 0, 0),
(365, 11, 5, 0, 0),
(366, 11, 6, 0, 0),
(367, 11, 7, 0, 0),
(368, 11, 8, 0, 0),
(369, 11, 9, 0, 0),
(370, 11, 10, 0, 0),
(371, 11, 11, 0, 0),
(372, 11, 12, 0, 0),
(373, 11, 13, 0, 0),
(374, 11, 14, 0, 0),
(375, 11, 15, 0, 0),
(376, 11, 16, 0, 0),
(377, 11, 17, 0, 0),
(378, 11, 18, 0, 0),
(379, 11, 19, 0, 0),
(380, 11, 20, 0, 0),
(381, 11, 21, 0, 0),
(382, 11, 22, 0, 0),
(383, 11, 23, 0, 0),
(384, 11, 24, 0, 0),
(385, 11, 25, 0, 0),
(386, 11, 26, 0, 0),
(387, 11, 27, 0, 0),
(388, 11, 28, 0, 0),
(389, 11, 29, 0, 0),
(390, 11, 30, 0, 0),
(391, 12, 1, 0, 0),
(392, 12, 2, 0, 0),
(393, 12, 3, 0, 0),
(394, 12, 4, 0, 0),
(395, 12, 5, 0, 0),
(396, 12, 6, 0, 0),
(397, 12, 7, 0, 0),
(398, 12, 8, 0, 0),
(399, 12, 9, 0, 0),
(400, 12, 10, 0, 0),
(401, 12, 11, 0, 0),
(402, 12, 12, 0, 0),
(403, 12, 13, 0, 0),
(404, 12, 14, 0, 0),
(405, 12, 15, 0, 0),
(406, 12, 16, 0, 0),
(407, 12, 17, 0, 0),
(408, 12, 18, 0, 0),
(409, 12, 19, 0, 0),
(410, 12, 20, 0, 0),
(411, 12, 21, 0, 0),
(412, 12, 22, 0, 0),
(413, 12, 23, 0, 0),
(414, 12, 24, 0, 0),
(415, 12, 25, 0, 0),
(416, 12, 26, 0, 0),
(417, 12, 27, 0, 0),
(418, 12, 28, 0, 0),
(419, 12, 29, 0, 0),
(420, 12, 30, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `automerkki`
--
ALTER TABLE `automerkki`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avainpaikat`
--
ALTER TABLE `avainpaikat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auto`
--
ALTER TABLE `auto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `automerkki`
--
ALTER TABLE `automerkki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `avainpaikat`
--
ALTER TABLE `avainpaikat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
