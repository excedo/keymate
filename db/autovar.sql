-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2017 at 04:17 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autovar`
--

-- --------------------------------------------------------

--
-- Table structure for table `auto`
--

CREATE TABLE `auto` (
  `id` int(11) NOT NULL,
  `reknr` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `merkki` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `malli` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `autopaikka` int(5) DEFAULT NULL,
  `avainpaikka` varchar(7) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `auto`
--

INSERT INTO `auto` (`id`, `reknr`, `merkki`, `malli`, `autopaikka`, `avainpaikka`, `status`, `deleted`) VALUES
(1, 'ABC-123', 'Mersuu', '110', NULL, NULL, NULL, 1),
(2, 'EFF-445', 'BMW', 'M3', NULL, NULL, NULL, 0),
(3, 'asdfasdf', 'volvo', 'asdfasdf', 0, 'asdfasd', 'volvo', 0),
(4, '', 'volvo', '', 0, '', 'volvo', 1),
(5, '', 'volvo', '', 0, '', 'volvo', 0),
(6, '', 'volvo', '', 0, '', 'volvo', 1),
(7, '', 'Volvo', '', 0, '', 'Vapaana varastossa<', 0),
(8, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(9, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(10, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(11, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(12, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 1),
(13, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 1),
(14, 'asdfasd', 'Volvo', 'dd', 0, 'd', 'Pesu', 1),
(15, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(16, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(17, 'ABC-123', 'Mersuu', '110', 12, 'K2-5', 'status', 0),
(18, 'EFF-445', 'BMW', 'M3', 5, 'K12-30', 'status', 0),
(19, 'asdfasdf', 'volvo', 'asdfasdf', 0, 'asdfasd', 'volvo', 0),
(20, '', 'volvo', '', 0, '', 'volvo', 0),
(21, '', 'volvo', '', 0, '', 'volvo', 0),
(22, '', 'volvo', '', 0, '', 'Korjaamolla', 0),
(23, '', 'Volvo', '', 0, '', 'Vapaana varastossa<', 0),
(24, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(25, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(26, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(27, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(28, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(29, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 1),
(30, 'asdfasd', 'Volvo', 'dd', 0, 'd', 'Pesu', 0),
(31, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 1),
(32, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(33, 'ABC-123', 'Mersuu', '110', 12, 'K2-5', 'status', 0),
(34, 'EFF-445', 'BMW', 'M3', 5, 'K12-30', 'status', 0),
(35, 'asdfasdf', 'volvo', 'asdfasdf', 0, 'asdfasd', 'volvo', 0),
(36, '', 'volvo', '', 0, '', 'volvo', 0),
(37, '', 'volvo', '', 0, '', 'volvo', 0),
(38, '', 'volvo', '', 0, '', 'volvo', 0),
(39, '', 'Volvo', '', 0, '', 'Vapaana varastossa<', 0),
(40, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(41, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(42, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(43, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(44, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(45, 'rdc-455', 'Fiat', 'Punto', NULL, NULL, NULL, 0),
(46, 'asdfasd', 'Volvo', 'dd', 0, 'd', 'Pesu', 0),
(47, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(48, 'asdfasdfasdfasdf', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(49, 'ABC-123', 'Mersuu', '110', 12, 'K2-5', 'status', 0),
(50, 'EFF-445', 'BMW', 'M3', 5, 'K12-30', 'status', 0),
(51, 'asdfasdf', 'volvo', 'asdfasdf', 0, 'asdfasd', 'volvo', 0),
(52, '', 'volvo', '', 0, '', 'volvo', 0),
(53, '', 'volvo', '', 0, '', 'volvo', 0),
(54, '', 'volvo', '', 0, '', 'volvo', 0),
(55, '', 'Volvo', '', 0, '', 'Vapaana varastossa<', 0),
(56, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(57, 'rdc-455', 'Fiat', 'Punto', 12, 'K3-5', 'Vapaana varastossa<', 0),
(58, 'dd', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(59, 'SS', 'Volvo', '', 0, '', 'Vapaana varastossa', 0),
(60, 'FRR-76', 'Saab', '555', 5, 'K5-7', 'Katsastus', 0),
(61, '', 'Citroï¿½n', '', 0, '', 'Katsastus', 0),
(62, 'RTT-999', 'Alfa Romeo', '', 0, '', 'Vapaana varastossa', 0),
(63, '', NULL, NULL, 0, NULL, NULL, 0),
(64, 'ASD', NULL, NULL, 9, NULL, NULL, 0),
(65, 'EED', NULL, NULL, 5, NULL, 'Varastossa', 0),
(66, '', NULL, NULL, 1, NULL, 'Varastossa', 0),
(67, 'DDD', NULL, NULL, 1, NULL, 'Varastossa', 0),
(68, 'EED-123', NULL, NULL, 8, NULL, 'Varastossa', 0),
(69, '', 'Alfa Romeo', '', NULL, NULL, NULL, 0),
(70, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(71, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(72, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(73, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(74, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(75, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(76, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(77, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(78, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(79, '', 'Alfa Romeo', '', NULL, NULL, NULL, 0),
(80, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(81, '', 'Alfa Romeo', '', 0, '', 'Varastossa', 0),
(82, 'ZZZ', 'Bentley', 'zz', 0, 'zz', 'Korjaamolla', 0),
(83, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(84, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(85, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(86, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(87, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(88, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(89, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(90, '', 'Alfa Romeo', '', 0, '', 'Koeajolla', 0),
(91, 'EDD-122', NULL, NULL, NULL, NULL, NULL, 0),
(92, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0),
(93, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0),
(94, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0),
(95, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0),
(96, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0),
(97, 'EDD-122', NULL, NULL, 1, NULL, 'Varastossa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `automerkki`
--

CREATE TABLE `automerkki` (
  `id` int(11) NOT NULL,
  `automerkki` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `del` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `automerkki`
--

INSERT INTO `automerkki` (`id`, `automerkki`, `del`) VALUES
(1, 'Alfa Romeo', NULL),
(2, 'Aston Martin ', NULL),
(3, 'Audi', NULL),
(4, 'Bentley', NULL),
(5, 'BMW', NULL),
(6, 'Chevrolet', NULL),
(7, 'Chrysler', NULL),
(8, 'Citroën', NULL),
(9, 'Dacia', NULL),
(10, 'Daewoo', NULL),
(11, 'Daihatsu', NULL),
(12, 'Dodge', NULL),
(13, 'Ferrari', NULL),
(14, 'Fiat', NULL),
(15, 'Ford', NULL),
(16, 'GMC', NULL),
(17, 'Honda', NULL),
(18, 'Hyundai', NULL),
(19, 'Isuzu', NULL),
(20, 'Jaguar', NULL),
(21, 'Jeep', NULL),
(22, 'Kia', NULL),
(23, 'Lada', NULL),
(24, 'Lamborghini', NULL),
(25, 'Lancia', NULL),
(26, 'Land Rover', NULL),
(27, 'Lexus', NULL),
(28, 'Lincoln', NULL),
(29, 'Lotus', NULL),
(30, 'Maserati', NULL),
(31, 'Maybach', NULL),
(32, 'Mazda', NULL),
(33, 'Mercedes-Benz', NULL),
(34, 'Mini', NULL),
(35, 'Mitsubishi', NULL),
(36, 'Nissan', NULL),
(37, 'Opel', NULL),
(38, 'Peugeot', NULL),
(39, 'Porsche', NULL),
(40, 'Renault', NULL),
(41, 'Rolls-Royce', NULL),
(42, 'Rover', NULL),
(43, 'Saab', NULL),
(44, 'Seat', NULL),
(45, 'Skoda', NULL),
(46, 'Smart', NULL),
(47, 'Subaru', NULL),
(48, 'Suzuki', NULL),
(49, 'Toyota', NULL),
(50, 'Volkswagen', NULL),
(51, 'Volvo', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `automerkki`
--
ALTER TABLE `automerkki`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auto`
--
ALTER TABLE `auto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `automerkki`
--
ALTER TABLE `automerkki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
