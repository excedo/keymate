<h2>Get data as JSON from a PHP file on the server.</h2>

<p id="demo"></p>

<script>

var xmlhttp = new XMLHttpRequest();

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        myObj = JSON.parse(this.responseText);
        myObj = myObj[0]
        document.getElementById("demo").innerHTML = myObj.reknr;
        //console.log( myObj);
    }
};
xmlhttp.open("GET", "geteditmodal.php?edit=1", true);
xmlhttp.send();

</script>
