<?php
require_once(__DIR__ . "/Medoo.php");
require_once(__DIR__ . '/db.php');
require_once(__DIR__ . '/config.php');
if (isset($_GET['edit']) && 0 < intval($_GET['edit'])) {
    $modaledit_data = $db->select($db_auto, ['id','reknr','autopaikka','avainpaikka','status'], ["id" => intval($_GET['edit'])]);
    $myJSON = json_encode($modaledit_data);
    die($myJSON);
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $edit_id = $_POST['edit-id'];
    $autopaikka = $_POST['autopaikka'];
    $avainpaikka = $_POST['avainpaikka'];
    $status = $_POST['status'];

    if ($status == "Korjaamolla") {
      $db->update($db_auto, [
        'autopaikka' => null,
        'avainpaikka' => null,
        'status' => $status
        ], [
        "id" => $edit_id
        ]);

        // $avainpaikka_edit_id = $db->select($db_autopaikkat, [
        // 	"id"
        // ], [
        // 	"auto_id" => $edit_id
        // ]);
        //echo '<pre>'; print_r($avainpaikka_edit_id); echo '</pre>';
        //echo "<br>" . "avainpaikka 0 id: " . $avainpaikka_edit_id[0]['id'];

        $db->update($db_autopaikkat, [
            'avaintila' => 0,
            'auto_id' => 0
            ], [
            'auto_id' => $edit_id
            ]);
    //var_dump( $db->error() );
    }

    elseif ($status == "Koeajossa") {
      $db->update($db_auto, [
        'autopaikka' => null,
        'avainpaikka' => null,
        'status' => $status
        ], [
        "id" => $edit_id
        ]);

        // $avainpaikka_edit_id = $db->select($db_autopaikkat, [
        //   "id"
        // ], [
        //   "auto_id" => $edit_id
        // ]);

        $db->update($db_autopaikkat, [
            'avaintila' => 0,
            'auto_id' => 0
            ], [
            'auto_id' => $edit_id
            ]);
    //var_dump( $db->error() );
    }
    else {
      $db->update($db_auto, [
        'autopaikka' => $autopaikka,
        'status' => $status
        ], [
        "id" => $edit_id
        ]);
    }
    echo "Ok";
    header("Location: index.php");
    exit;
}
