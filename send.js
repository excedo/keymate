function editRow(id) {
  console.log(id);
  if ('undefined' != typeof id) {
    $.getJSON('geteditmodal.php?edit=' + id, function(obj) {
      obj = obj[0]
      console.log(obj)
      $("#edit_print_rek").text(obj.reknr);
      $("#edit_print_kja").text(obj.avainpaikka);
      $("#edit_print_kap").text(obj.autopaikka);
      $("input[id=edit-id]").val(obj.id);
      $("input[id=autopaikka]").val(obj.autopaikka);
      $("select[id=status]").val(obj.status);
      $('#edit').modal('show')
    }).fail(function() {
      console.log('Unable to fetch data, please try again later.')
    });
  } else console.log('Unknown row id.');
}
