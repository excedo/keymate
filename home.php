<?php
// Viktor Löfstedt
// While in the employment of
// Pritek Systems Oy
require_once(__DIR__. "/navigation.php");
echo '<script>document.getElementById("nav_li_home").class = "active";';
echo 'document.getElementById("nav_li_hall").class = ""</script>';
//include "/geteditmodal.php";
//include "/edit_ajax.php";

//include all modals html
include "./home_modals.php";
      //basic table db fetch
      $data = $db->select($db_auto, [
      'reknr',
      'autopaikka',
      'avainpaikka',
      'status',
      'avain_maara',
      'id'
  ]);
  //id table fetch
  //$id = $db->select($db_auto, [
  //    'id'
  //  ]);
  //'<pre>'; print_r($avain_data); echo '</pre>';
  //echo $data[$rivien_maara]['status'];
  //echo $data[0]['reknr'];
  //include(__DIR__. "/edit_print_del.php");
  //include(__DIR__. "/paluumodal.php");
?>
<div class="container color-otsikko-pihavarasto">
  <h1>Pihavarasto <small class="color-primary-0">avainpaikkojen apuri</small></h1>
</div>

<div class="container bg-pihavarasto well">
  <div class="row">
    <div class="col-sm-8">
      <form action="#" method="get">
        <div class="input-group input-group-lg">
          <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
          <input class="form-control etsi-fonttikoko" id="system-search" name="q" placeholder="Etsi esim. abc- tai -123" required autofocus>
          <span class="input-group-btn">
                      <a href="#system-search" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                  </span>
        </div>
      </form>
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-2">
      <button type="button" class="btn btn-success btn-lg" data-toggle="modal" id="iso_lisaa_auto_btn" data-loading-text="<span class='glyphicon-left glyphicon glyphicon-refresh spinning'</span>" data-target="#lisaa_auto"><span class="glyphicon glyphicon-plus"></span> Lisää Auto</button>
    </div>
    <div class="col-sm-1">
    </div>
  </div>

      <div class="table-responsive">
        <table class="table table-list-search table-hover">
          <?php if (count($data) > 0): ?>
          <thead class="color-thead-pihavarasto">
            <tr>
              <th>Rekisterinumero</th>
              <th>Autorivi</th>
              <th>Kaappi-Paikka</th>
              <th>Tila</th>
              <th><span class="glyphicon glyphicon-tags"></span></th>
              <th>&nbsp</th>
            </tr>
          </thead>
          <tbody class="color-tbody-pihavarasto">
            <?php
    $rivien_maara = 0;
    foreach ($data as $row): array_map('htmlentities', $row);
           if (strpos($data[$rivien_maara]['status'], 'Varastossa') !== false) {
            echo '<tr class="bg-success edit_data">';
          } elseif (strpos($data[$rivien_maara]['status'], 'Korjaamolla') !== false) {
              echo '<tr class="bg-info edit_data">';
          } else {
              echo '<tr class="bg-warning  edit_data">';
          }
          echo "<td><h4>" . $data[$rivien_maara]['reknr'] . "</h4></td>";
          if ((is_null($data[$rivien_maara]['autopaikka']))) {
          echo "<td>-</td><td>-</td>";
          }
          else {
          echo "<td><h4>" . $data[$rivien_maara]['autopaikka'] . ". Rivi</h4></td>";
          echo "<td><h4>K" . $data[$rivien_maara]['autopaikka'] . "-" . $data[$rivien_maara]['avainpaikka'] . "</h4></td>";
          }
          echo "<td><h4>" . $data[$rivien_maara]['status'] . "</h4></td>";
          echo "<td>" . $data[$rivien_maara]['avain_maara'] . "</td>";
          if (strpos($data[$rivien_maara]['status'], 'Varastossa') !== false) {
            echo '<td><div class="btn-toolbar"><button id="koeajo_btn' . $data[$rivien_maara]['id'] . '" data-loading-text="';
            echo '<span class=\'glyphicon-left glyphicon glyphicon-refresh spinning\'</span>"';
            echo ' class="btn btn-default btn-md btn-warning" onclick="koeajoon(';
            echo "'";

            // data-loading-text="<span class=\'glyphicon-left glyphicon glyphicon-refresh spinning\'</span>"

            echo $data[$rivien_maara]['id'];
            echo "')";
            echo '"><span class="glyphicon glyphicon-dashboard"> Koeajoon</span></button>';
            echo '<button id="korjaamo_btn' . $data[$rivien_maara]['id'] . '" class="btn btn-default btn-md  btn-info" data-loading-text="<span class=\'glyphicon-left glyphicon glyphicon-refresh spinning\'</span>" onclick="korjaamolle(';
            echo "'";
            echo $data[$rivien_maara]['id']; echo "')"; echo '"><span class="glyphicon glyphicon-wrench"> Korjaamolle</span></button></td>';
            echo "</div></tr>";
          }
          else {
            if (strpos($data[$rivien_maara]['status'], 'Koeajossa') !== false) {
              echo '<td><button id="paluu_btn' . $data[$rivien_maara]['id'] . '" class="btn btn-default btn-md  btn-success" data-loading-text="<span class=\'glyphicon-left glyphicon glyphicon-refresh spinning\'</span>" onclick="paluu(';
              echo "'";
              echo $data[$rivien_maara]['id']; echo "')"; echo '"><span class="glyphicon glyphicon-ok"> Varastoon</span></button>  ';
              echo '<button id="myy_auto" class="btn btn-default btn-md  btn-danger" onclick="myyAuto(';
              echo "'";
              echo $data[$rivien_maara]['id']; echo "')"; echo '"><span class="glyphicon glyphicon-eur"> Myyty</span></button></td></tr>';
            }
            else {
              echo '<td><button id="paluu_btn' . $data[$rivien_maara]['id'] . '" data-loading-text="<span class=\'glyphicon-left glyphicon glyphicon-refresh spinning\'</span>" class="btn btn-default btn-md  btn-success" onclick="paluu(';
              echo "'";
              echo $data[$rivien_maara]['id']; echo "')"; echo '"><span class="glyphicon glyphicon-ok"> Varastoon</span></button></td></tr>';
            }
          }

                $rivien_maara++;
                endforeach; ?>
          </tbody>
        </table>
        <p class="color-thead-pihavarasto">Autoja:
          <?php echo $rivien_maara ?>
        </p>
      <?php endif; ?>
      </div>
    </div>
