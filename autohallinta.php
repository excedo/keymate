<?php
// Viktor Löfstedt
// While in the employment of
// Pritek Systems Oy
require_once(__DIR__. "/navigation.php");
echo '<script>document.getElementById("nav_li_home").class = "";</script>';
echo '<script>document.getElementById("nav_li_hall").class = "active";</script>';
//include "/geteditmodal.php";
//include "/edit_ajax.php";

//include all modals html
include "./home_modals.php";
?>
<div class="container color-otsikko-ahallinta">
  <h1>Autohallinta <small class="color-otsikko-ahallinta">muokkaa ja poista autoja</small></h1>
</div>
<div class="container bg-autohallinta well ">
  <div class="row">
    <div class="col-sm-8">
      <form action="#" method="get">
        <div class="input-group">
          <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
          <input class="form-control" id="system-search" name="q" placeholder="Etsi esim. abc- tai -123" required autofocus>
          <span class="input-group-btn">
                      <a href="#system-search" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a>
                  </span>
        </div>
      </form>
    </div>
    <div class="col-sm-2">
    </div>
    <div class="col-sm-2">
      <!-- <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#lisaa_auto"><span class="glyphicon glyphicon-plus"></span> Lisää Auto</button> -->
    </div>
  </div>

    <?php
    $data = $db->select($db_auto, [
    'reknr',
    'autopaikka',
    'avainpaikka',
    'status',
    'avain_maara',
    'timestamp',
    'id'
    ]);
    $today = date("Y-m-d H:i:s");
    //$id = $db->select($db_auto, [
    //    'id'
    //  ]);
    //'<pre>'; print_r($avain_data); echo '</pre>';
    //echo $data[$rivien_maara]['status'];
    //echo $data[0]['reknr'];
    ?>


      <div class="table-responsive">
        <table class="table table-list-search table-hover">
          <?php if (count($data) > 0): ?>
          <thead class="color-thead-ahallinta">
            <tr>
              <th>Rekisterinumero</th>
              <th>Autorivi</th>
              <th>Kaappi-Paikka</th>
              <th>Tila</th>
              <th><span class="glyphicon glyphicon-time"></span></th>
              <th class="hidden">Id</th>
              <th><span class="glyphicon glyphicon-tags"></span></th>
              <th>&nbsp</th>
            </tr>
          </thead>
          <tbody class="color-tbody-ahallinta">
            <?php
            $rivien_maara = 0;
            foreach ($data as $row): array_map('htmlentities', $row);
              $muutosaika = new DateTime($data[$rivien_maara]['timestamp']);
              $nykyhetki = new DateTime($today);
              $interval0 = $muutosaika->diff($nykyhetki);
              $diffo_h = $interval0->h;
              $extrahour = round($interval0->i/60, 0,PHP_ROUND_HALF_UP);
              $diffo_h = $diffo_h + ($interval0->days*24) + $extrahour;
              $diffo_p = $interval0->d;
              $extraday = round($interval0->h/24, 0,PHP_ROUND_HALF_UP);
              $diffo_p = $diffo_p + $extraday;



           if (strpos($data[$rivien_maara]['status'], 'Varastossa') !== false) {
            echo '<tr class="bg-success edit_data" <a onclick="javascript:editRow('; echo $data[$rivien_maara]['id'];echo ');" </a>';
          } elseif (strpos($data[$rivien_maara]['status'], 'Korjaamolla') !== false) {
            if ($diffo_p >= 4) {
            echo '<tr class="bg-danger  edit_data" <a onclick="javascript:editRow('; echo $data[$rivien_maara]['id'];echo ');" </a>';
            }
            else {
              echo '<tr class="bg-info edit_data" <a onclick="javascript:editRow('; echo $data[$rivien_maara]['id'];echo ');" </a>';
            }
          } else {
            if ($diffo_h >= 12) {
            echo '<tr class="bg-danger  edit_data" <a onclick="javascript:editRow('; echo $data[$rivien_maara]['id'];echo ');" </a>';
            }
            else {
              echo '<tr class="bg-warning  edit_data" <a onclick="javascript:editRow('; echo $data[$rivien_maara]['id'];echo ');" </a>';
            }

          }
              echo "<td>" . $data[$rivien_maara]['reknr'] . "</td>";
              if ((is_null($data[$rivien_maara]['autopaikka']))) {
              echo "<td></td><td></td>";
              }
              else {
              echo "<td>" . $data[$rivien_maara]['autopaikka'] . ". Rivi</td>";
              echo "<td>K" . $data[$rivien_maara]['autopaikka'] . "-" . $data[$rivien_maara]['avainpaikka'] . "</td>";
              }
              echo "<td>" . $data[$rivien_maara]['status'] . "</td>";


                  if (strpos($data[$rivien_maara]['status'], 'Koeajossa') !== false ) {
                                  if ($diffo_h == 1) {
                                    echo "<td>" . $diffo_h . " tunti</td>";
                                                  }
                                  else {
                                    echo "<td>" . $diffo_h . " tuntia</td>";
                                        }
                                  }
                    else {

                                  if ($diffo_p == 1) {
                                      echo "<td>" . $diffo_p . " päivä</td>";
                                                  }
                                  else {
                                      echo "<td>" . $diffo_p . " päivää</td>";
                                        }
                                  }
                ?>
                <td class="hidden"><?php echo $data[$rivien_maara]['id']?></td>
                <td><?php echo $data[$rivien_maara]['avain_maara']?></td>
                <td><button id="del_button<?php echo $data[$rivien_maara]['id']?>" class="MetroBtn" data-loading-text="<span class='glyphicon-left glyphicon glyphicon-refresh spinning'</span>" onclick="delRow('<?php echo $data[$rivien_maara]['id']?>')"><span class="glyphicon glyphicon-trash"></span></button></td></tr>
                <?php $rivien_maara++ ?>
                <?php endforeach; ?>
          </tbody>
        </table>
      </div>
        <p class="color-thead-ahallinta">Autoja:
          <?php echo $rivien_maara ?>
        </p>
    <?php endif; ?>
  </div>
